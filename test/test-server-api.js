var getData = require('../server/api-server');
var assert = require('assert');
var request = require('request');
var beers = require('../server/beers.mock');

_EXTERNAL_URL = "https://temperature-sensor-service.herokuapp.com/sensor/";
temp_range = [-2, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10];


describe('Get json object with all beers', function () {
  this.timeout(10000);
  it('Should respond with a json object with all beers', function testSlash(done) {
    let result;
    request({ uri: "http://localhost:3000/beers/temperature", json: true }, function (error, response, body) {
      result = (body);
      assert.equal(result.length, 6);
      for (var i = 0; i < result.length; i++) {
        assert.equal(result[i].hasOwnProperty('id'), true, "Object should have a id property");
        assert.equal(result[i].hasOwnProperty('beer'), true), "Object should have a beer property";
        assert.equal(result[i].hasOwnProperty('range_min'), true, "Object should have a range_min property");
        assert.equal(result[i].hasOwnProperty('range_max'), true, "Object should have a range_max property");
        assert.equal(result[i].hasOwnProperty('actual_temperature'), true, "Object should have a actual_temperature property");
        assert.equal(result[i].hasOwnProperty('alert'), true, "Object should have a alert property");
        assert.equal(result[i].hasOwnProperty('imgUrl'), true, "Object should have a imgUrl property");
      }

      done();
    });
  });

  it('404 everything else', function testPath(done) {
    request({ uri: "http://localhost:3000/", json: true }, function (error, response, body) {
      result = (body);
      assert.equal(response.statusCode, 404);
      done();
    });
  });

  it('404 everything else', function testPath(done) {
    request({ uri: "http://localhost:3000/foo", json: true }, function (error, response, body) {
      result = (body);
      assert.equal(response.statusCode, 404);
      done();
    });
  });

  it('404 everything else', function testPath(done) {
    request({ uri: "http://localhost:3000/foo/bar", json: true }, function (error, response, body) {
      result = (body);
      assert.equal(response.statusCode, 404);
      done();
    });
  });

});



describe('Get temperature from the API', function () {
  this.timeout(100000);


  it('Testing for positive numbers', async () => {
    let result = await getData(_EXTERNAL_URL, 1)
    assert.equal(result.id, 1, "Id is not correct")
    value = temp_range.indexOf(result.temperature);
    assert.notStrictEqual(value, -1, "Temperature is out of the default range")
    // flawed test throws an error, but it is not a test error (AssertionError)
  });

  it('Testing for negative numbers', async () => {
    let result = await getData(_EXTERNAL_URL, -10);
    assert.equal(result.id, -10, "Id is not correct");
    value = temp_range.indexOf(result.temperature);
    assert.notStrictEqual(value, -1, "Temperature is out of the default range");
  });

  // {"statusCode":404,"error":"Not Found","message":"Not Found"}
  it('Testing for empty id', async () => {
    let result = await getData(_EXTERNAL_URL, "");
    assert.equal(result.statusCode, 404);
    assert.equal(result.error, "Not Found");
    assert.equal(result.message, "Not Found");
    // flawed test throws an error, but it is not a test error (AssertionError)
  });

  it('Testing for big id', async () => {
    let result = await getData(_EXTERNAL_URL, 99999999999);
    assert.equal(result.id, 99999999999, "Id is not correct");
    value = temp_range.indexOf(result.temperature);
    assert.notStrictEqual(value, -1, "Temperature is out of the default range");
    // flawed test throws an error, but it is not a test error (AssertionError)
  });

  it('Testing for strings', async () => {
    let result = await getData(_EXTERNAL_URL, "testing")
    assert.notDeepEqual(result, { id: 'undefined', temperature: new Number });
    // flawed test throws an error, but it is not a test error (AssertionError)
  });


});



