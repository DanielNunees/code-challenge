const express = require('express')
const cors = require('cors')
const app = express()
const axios = require("axios");
let _beers = require('./beers.mock')

_EXTERNAL_URL = "https://temperature-sensor-service.herokuapp.com/sensor/";
const getData = async (_EXTERNAL_URL, id) => {
    try {
        const response = await axios.get(_EXTERNAL_URL + id);
        const data = response.data;
        return data;
    } catch (error) {
        if (error.response)
            return error.response.data;
        return error.message;
    }
};

const checkBeerTemperature = (array) => {
    let final_array = [];
    array.sort((a, b) => (a.id > b.id) ? 1 : ((b.id > a.id) ? -1 : 0))
        .forEach(element => {
            element.alert = false;
            if (element.actual_temperature > element.range_max) {
                element.alert = true;
            }
            if (element.actual_temperature < element.range_min) {
                element.alert = true;
            }
            final_array.push(element);
        });
    return final_array;
}

app.get('/beers/temperature', cors(), (req, res, next) => {
    let array = [];
    const cloneBeers = Array.from(_beers);
    cloneBeers.forEach(function (beer) {
        beer.actual_temperature = null;
        getData(_EXTERNAL_URL, beer.id).then(api_response => {
            beer.actual_temperature = api_response.temperature;
            array.push(beer);
            if (array.length === beers.length) {
                response = checkBeerTemperature(array);
                res.json(response);
            }
        })
    });
})

const server = app.listen(3000, () => {
    console.log('Listening on port %s', server.address().port)
})

module.exports = server;
module.exports = getData;
