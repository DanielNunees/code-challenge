module.exports = beers = [
    {
        'id': 1,
        "beer": "Pilsner",
        'range_min': 4,
        'range_max': 6,
        'actual_temperature': null,
        'alert': false,
        'imgUrl': "https://products2.imgix.drizly.com/ci-old-style-pilsner-6de3bfffa3301563.png?auto=format%2Ccompress&fm=jpeg&q=20"
    },
    {
        'id': 2,
        "beer": "IPA",
        'range_min': 5,
        'range_max': 6,
        'actual_temperature': null,
        'alert': false,
        'imgUrl': "https://www.totalwine.com/dynamic/490x/media/sys_master/twmmedia/ha2/h61/8802858860574.png"

    },
    {
        'id': 3,
        "beer": "Lager",
        'range_min': 4,
        'range_max': 7,
        'actual_temperature': null,
        'alert': false,
        'imgUrl': 'https://products0.imgix.drizly.com/ci-yuengling-lager-6f82d7ee4e6dba1a.png?auto=format%2Ccompress&dpr=2&fm=jpeg&h=240&q=20'
    },
    {
        'id': 4,
        "beer": "Stout",
        'range_min': 6,
        'range_max': 8,
        'actual_temperature': null,
        'alert': false,
        'imgUrl': 'https://www.sambrooksbrewery.co.uk/wp-content/uploads/2017/12/stout-2.jpg'
    },
    {
        'id': 5,
        "beer": "Wheat beer",
        'range_min': 3,
        'range_max': 5,
        'actual_temperature': null,
        'alert': false,
        'imgUrl': 'https://germanliquor.com.au/wp-content/uploads/2016/11/Maisels-Original-fn.png'
    },
    {
        'id': 6,
        "beer": "Pale Ale",
        'range_min': 4,
        'range_max': 6,
        'actual_temperature': null,
        'alert': false,
        'imgUrl': 'https://www.sambrooksbrewery.co.uk/wp-content/uploads/2017/12/pale-ale-2.jpg'
    },
]