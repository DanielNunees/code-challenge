# Code Challenge

## What are the highlights of my logic/code writing style?
- I have reduced the number of dependencies to only 5 (production mode)

- The system files sizes it's less than 1MB (772kb)


## What could have been done in a better way? What would you do in version 2.0?
- In the version 2.0 would be included a time range to the temperature stay outside the range, because the temperature variation is too high and completely random and i believe that just will be a problem if the beers stay a long time out of the range.

## What were the questions you would ask and your own answers/assumptions?
- I have assumed that the driver need to see all the information togheter in just one screen, and the temperatures need to be updated at the same in a small period of time.

- I have assumed that every beer has it own ID, (1..6)

- I have assumed that the information at the mock file, will come from a database and it's possible to change the temperature range, beer types and image without touch on the front-end.


## Any other notes you feel relevant for the evaluation of your solution.
- The system was tested in two differents environments (Linux Ubuntu 18.04 and Mac Os Mojave)

- The system was tested with two different Node js versions
    --10.15.1
    --9.8.0

## Environment Dependencies
 - Node.js
 - NPM

## Development instructions mode

- Download the repository
- `cd code-challenge-pragma.team`
- `npm install`
- `npm start`
- Open the browser on localhost:4200

## Deploy
- `ng build --prod --base-href /code-challenge/`

## Test Dependencies
 - Node.js

 ## Test instructions
 - Download the repository
 - `cd code-challenge-pragma.team`
 - `npm install` if you haven't ran yet
 
 - `npm test`
 or
 - `cd dist/`
 - `npm test`

## Production mode instructions

- Download the repository
- `cd code-challenge-pragma.team`
- `cd dist/`
- `npm install`
- `npm start`
- Open the browser on localhost:3001
