const express = require('express')
const cors = require('cors')
const app = express()
const axios = require("axios");
let beers = require('./beers.mock')

_EXTERNAL_URL = "https://temperature-sensor-service.herokuapp.com/sensor/";




const getData = async (_EXTERNAL_URL, id) => {
    try {
        const response = await axios.get(_EXTERNAL_URL + id);
        const data = response.data;
        return data;
    } catch (error) {
        console.log(error);
    }
};

app.get('/beers/temperature', cors(), (req, res, next) => {
    array = [];
    beers.forEach(function (beer) {
        getData(_EXTERNAL_URL, beer.id).then(api_response => {
            beer.actual_temperature = api_response.temperature;
            array.push(beer);
            if (array.length === beers.length)
                res.json(array.sort((a, b) => (a.id > b.id) ? 1 : ((b.id > a.id) ? -1 : 0))
                );
        })
    });
})




const server = app.listen(3000, () => {
    console.log('Listening on port %s', server.address().port)
})