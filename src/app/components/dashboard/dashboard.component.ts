import { Component, OnInit } from '@angular/core';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  beers;
  frequency = 1;


  constructor(private us: UsersService) { }
  ngOnInit() {
    this.setTime();
  }

  setTime() {
    this.uptadeTemperatures();
    setInterval(() => {
      this.uptadeTemperatures();
    }, this.frequency * 60000);
  }

  uptadeTemperatures() {
    this.us.getTemperature().subscribe(response => {
      this.beers = response;
    });
  }


}
